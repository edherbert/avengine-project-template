function start(){
    local width = _window.getWidth();
    local height = _window.getHeight();

    //Create logo
    {
        ::logo <- MovableTexture("logo.png");
        ::logo.setSize(width * 0.75, height * 0.75);
        ::logo.setPosition(width * 0.12, height * 0.025);
    }

    //Create text
    {
        local winSize = Vec2(width, height);

        _gui.setCanvasSize(winSize, winSize);

        ::window <- _gui.createWindow();
        ::window.setSize(winSize);
        ::window.setVisualsEnabled(false);
        ::window.setClipBorders(0, 0, 0, 0);

        ::label <- window.createLabel();
        ::label.setDefaultFontSize(20);
        ::label.setTextHorizontalAlignment(_TEXT_ALIGN_CENTER);

        local v = _settings.getEngineVersion();
        ::label.setText(format("avEngine\n v%i.%i.%i %s", v.major, v.minor v.patch, v.suffix));

        ::label.setSize(width, label.getSize().y);
        ::label.setPosition(0, height - label.getSize().y);
    }

}

function update(){

}

function end(){

}